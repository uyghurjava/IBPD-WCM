package com.ibpd.henuocms.service.fun;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.entity.FunEntity;
@Transactional
@Service("funService")
public class FunServiceImpl extends BaseServiceImpl<FunEntity> implements IFunService {
	public FunServiceImpl(){
		super();
		this.tableName="FunEntity";
		this.currentClass=FunEntity.class;
		this.initOK(); 
	}

	public void init() {
		String funString="";
		String[] funs=funString.split("\r");
		for(String fun:funs){
			String[] fs=fun.split("\t");
			if(fs.length==3){
				String fNo=fs[0];
				String fName=fs[1];
				String fWebName=fs[2];
				FunEntity fe=new FunEntity();
				fe.setFunNo(fNo);
				fe.setFunName(fName);
				fe.setWebName(fWebName);
				fe.setIsLocked(false);
				this.saveEntity(fe);
			}
		}
	}
}
