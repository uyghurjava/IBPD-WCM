package com.ibpd.henuocms.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * 类功能说明 : 涉及时间的操作的工具类函数，主要侧重于日期时间的计算功能
 * 
 * 1、时间的格式化输出
 * 2、时间字符串与date之间的格式转换
 * 3、计算给定两个date之间的天数
 * 4、计算某一月中的天数
 * 5、date和calendar之间的转换
 * 6、获取当前系统的时间
 * 
 * @Description:
 * @author FWW MG
 * @date 2015年1月2日 下午4:59:11
 */
public class TimeHelper {

	public static void main(String[] args) {
		String date1 = "2013-5-20 12:23:23";
		String date2 = "2013-3-26 12:23:23";
		System.out.println(getDaysBetween(strToDate(date1, "yyyy-MM-dd HH:mm:ss"), strToDate(date2, "yyyy-MM-dd HH:mm:ss")));
	}
	
	/**
	 * 函数功能说明: 计算任意两天之间的天数间隔（不推荐使用）
	 * 			可以参考使用getDaysBetween方法
	 * 修改者名字:  fuweiwei mg
	 * 
	 * @param startDay
	 * @param endDay
	 * @return
	 */
	public static int getIntervalDays(Calendar startDay,Calendar endDay){
		if(null!=startDay && null!=endDay){
			if(startDay.after(endDay)){
				Calendar cal = startDay;
				startDay = endDay;
				endDay = cal;
			}
			long startLong = startDay.getTimeInMillis();
			long endLong = endDay.getTimeInMillis();
			long instance = startLong - endLong;
			return (int) (instance/(1000*60*60*24));
		}else{
			return -1;
		}
	}
	/**
	 * 函数功能说明: 计算任意两天之间的天数间隔 （不推荐使用）
	 * 			可以参考使用getDaysBetween方法
	 * @param startDay
	 * @param endDay
	 * @return
	 */
	public static int getIntervalDays(Date startDay,Date endDay){
		if(null!=startDay && null!=endDay){
			if(startDay.after(endDay)){
				Date date = startDay;
				startDay = endDay;
				endDay = date;
			}
			long startLong = startDay.getTime();
			long endLong = endDay.getTime();
			long instance = startLong - endLong;
			return (int) (instance/(1000*60*60*24));
		}else{
			return -1;
		}
	}
	
	/**
	 * 函数功能说明: 改进的计算两天之间的天数
	 * @param day1
	 * @param day2
	 * @return
	 * 		如果是非法的传输传入的话，返回的是-1
	 */
	public static int getDaysBetween(Calendar day1,Calendar day2){
		if(null!=day1 && null!=day2){
			if(day1.after(day2)){
				Calendar swap = day1;
				day1 = day2;
				day2 = swap;
			}
			int days = day2.get(Calendar.DAY_OF_YEAR) - day1.get(Calendar.DAY_OF_YEAR);
			int y2 = day2.get(Calendar.YEAR);
			if(day1.get(Calendar.YEAR)!=y2){
				day1 = (Calendar) day1.clone();
				do{
					days += day1.getActualMaximum(Calendar.DAY_OF_YEAR);
					day1.add(Calendar.YEAR, 1);
				}while(day1.get(Calendar.YEAR)!=y2);
			}
			return days;
		}else{
			return -1;
		}
	}
	
	/**
	 * 函数功能说明: 改进的计算两天之间的天数
	 * @param day1
	 * @param day2
	 * @return 如果是非法的传输传入的话，返回的是-1
	 */
	public static int getDaysBetween(Date day1,Date day2){
		return getDaysBetween(DateToCal(day1), DateToCal(day2));
	}
	/**
	 * 函数功能说明: 日历格式转换为日期格式
	 * @param calendar
	 * @return
	 */
	public static Date CalToDate(Calendar calendar){
		if(null != calendar){
			return calendar.getTime();
		}else{
			return null;
		}
	}
	
	/**
	 * 函数功能说明: 日期转换为日历的格式

	 * @param date
	 * @return
	 */
	public static Calendar DateToCal(Date date){
		if(null != date){
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return calendar;
		}else{
			return null;
		}
	}
	
	/**
	 * 函数功能说明: 依据给定的年和月份计算出来当月的天数
	 * @param year 给定的年份，四位的数字字符串
	 * @param month 给定的月份，月份
	 * @return
	 * 		如果格式是正确的，返回天数
	 * 		如果格式是不正确的，返回的是0
	 */
	public static int getTotalDays(String year,String month){
		if(Pattern.matches("^\\d{4}$", year) && Pattern.matches("^(0?[1-9]|1[0-2])$", month)){
			Calendar time = Calendar.getInstance();
			time.clear();
			time.set(Calendar.YEAR, Integer.parseInt(year));
			time.set(Calendar.MONTH, Integer.parseInt(month)-1);
			return time.getActualMaximum(Calendar.DAY_OF_MONTH);
		}else{
			return 0;
		}
	}
	
	/**
	 * 函数功能说明: 依据给定的时间转换为yyyy-MM-dd HH:mm:ss格式的时间字符串
	 * @param date 给定的时间
	 * @return
	 */
	public static String getDate(Date date){
		return getDate(date, "yyyy-MM-dd HH:mm:ss");
	}
	
	/**
	 * 函数功能说明: 依据给定的时间和日期的格式进行的转换
	 * @param date 给定的时间
	 * @param formatStr 要转换成为的格式
	 * @return
	 */
	public static String getDate(Date date,String formatStr){
		if(date!=null){
			if(null!=formatStr && ""!=formatStr.trim()){
				SimpleDateFormat simpleDateFormat;
				String dateStr = null;
				try {
					simpleDateFormat = new SimpleDateFormat(formatStr);
					dateStr = simpleDateFormat.format(date);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return dateStr;
			}else{
				IbpdLogger.getLogger(TimeHelper.class).info("给定的转换格式是空的！");
				return null;
			}
		}else{
//			System.out.println("给定的时间是空的！");
			return null;
		}
	}
	/**
	 * 函数功能说明: 根据一个给定的格式formatStr转换dateStr为日期格式
	 * @param dateStr 日期的字符串
	 * @param formatStr 日期的字符串格式
	 * @return
	 */
	public static Date strToDate(String dateStr,String formatStr){
		if(null!=dateStr&&dateStr.trim()!=""){
			if(null!=formatStr&&""!=formatStr.trim()){
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatStr);
				try {
					Date date = simpleDateFormat.parse(dateStr);
					return date;
				} catch (ParseException e) {
					e.printStackTrace();
					return null;
				}
			}else{
				IbpdLogger.getLogger(TimeHelper.class).info("没有提供转换的格式！");
				return null;
			}
		}else{
//			System.out.println("给定的时间的字符串是空的！");
			return null;
		}
		
	}
	 
	/**
	 * 函数功能说明: 获取当前的系统时间，格式yyyy-MM-dd HH-mm-ss
	 * @return
	 */
	public static String getNowDate(){
		return getDate(new Date());
	}
	
	/**
	 * 函数功能说明: 获取当前系统的年月日
	 * @return
	 */
	public static String getNowCal(){
		return getDate(new Date(), "yyyy-MM-dd");
	}
	
}
