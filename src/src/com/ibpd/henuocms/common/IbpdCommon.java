package com.ibpd.henuocms.common;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jsoup.helper.StringUtil;

import com.ibpd.common.ClassPath;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.entity.NodeAttrEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.entity.StyleTemplateEntity;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.entity.SystemProperEntity;
import com.ibpd.henuocms.service.SystemProper.ISystemProperService;
import com.ibpd.henuocms.service.SystemProper.SystemProperServiceImpl;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
import com.ibpd.henuocms.service.nodeAttr.INodeAttrService;
import com.ibpd.henuocms.service.nodeAttr.NodeAttrServiceImpl;
import com.ibpd.henuocms.service.pageTemplate.IPageTemplateService;
import com.ibpd.henuocms.service.pageTemplate.PageTemplateServiceImpl;
import com.ibpd.henuocms.service.styleTemplate.IStyleTemplateService;
import com.ibpd.henuocms.service.styleTemplate.StyleTemplateServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;
public class IbpdCommon {
	public static IbpdCommon getInterface(){
		return new IbpdCommon();
	}
	private String getSubSiteDir(Long siteId){
		ISubSiteService siteServ=(ISubSiteService) ServiceProxyFactory.getServiceProxy(SubSiteServiceImpl.class);
		SubSiteEntity site=siteServ.getEntityById(siteId);
		if(site==null)
			return "";
		return site.getDirectory();
	}
	public String getSubSiteUrlDir(Long siteId){
		String dir=getSubSiteDir(siteId);
		return "/sites/"+dir;
	}
	public String getSubSiteRealDir(Long siteId){
		String dir=getSubSiteDir(siteId);;
		return FileUtil.getWebRootDirPath()+File.separator+"sites"+File.separator+dir;
	}
	private String getNodeDir(Long nodeId){
		if(nodeId==null)
			return "";
		INodeAttrService attrServ=(INodeAttrService) ServiceProxyFactory.getServiceProxy(NodeAttrServiceImpl.class);
		NodeAttrEntity attr=attrServ.getNodeAttr(nodeId);
		if(attr==null)
			return "";
		return attr.getDirectory();
	}
	public String getNodeUrlDir(Long nodeId){
		if(nodeId==null)
			return "";
		INodeService nodeServ=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
		NodeEntity ne=nodeServ.getEntityById(nodeId);
		if(ne==null)
			return "";
		String idpath=ne.getNodeIdPath();
		String[] ids=idpath.split(",");
		String url="";
		url=getSubSiteUrlDir(ne.getSubSiteId());
		for(String id:ids){
			if(StringUtil.isNumeric(id)){
				url=url+"/"+getNodeDir(Long.parseLong(id));
			}
		}
		return url;
	}
	public String getNodeRealDir(Long nodeId){
		if(nodeId==null)
			return "";
		INodeService nodeServ=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
		NodeEntity ne=nodeServ.getEntityById(nodeId);
		if(ne==null)
			return "";
		String idpath=ne.getNodeIdPath();
		String[] ids=idpath.split(",");
		String url="";
		url+=getSubSiteRealDir(ne.getSubSiteId());
		for(String id:ids){
			if(StringUtil.isNumeric(id)){
				url=url+File.separator+getNodeDir(Long.parseLong(id));
			}
		}
		return url;
	}
	public String  getStyleTemplateUrlDir(Long styleTemplateId){
		if(styleTemplateId==null)
			return "";
		IStyleTemplateService pts=(IStyleTemplateService) ServiceProxyFactory.getServiceProxy(StyleTemplateServiceImpl.class);
		StyleTemplateEntity pt=pts.getEntityById(styleTemplateId);
		return TemplateUtil.getTemplateFileUrlPath(pt.getTemplateFilePath());
	}
	public String getStyleTemplateRealDir(Long styleTemplateId){
		if(styleTemplateId==null)
			return "";
		IStyleTemplateService pts=(IStyleTemplateService) ServiceProxyFactory.getServiceProxy(StyleTemplateServiceImpl.class);
		StyleTemplateEntity pt=pts.getEntityById(styleTemplateId);
		return TemplateUtil.getTemplateFileRealPath(pt.getTemplateFilePath());
	}
	public String getPageTemplateUrlDir(Long  pageTemplateId){
		if(pageTemplateId==null)
			return "";
		IPageTemplateService pts=(IPageTemplateService) ServiceProxyFactory.getServiceProxy(PageTemplateServiceImpl.class);
		PageTemplateEntity pt=pts.getEntityById(pageTemplateId);
		return getPageTemplateUrlDir(pt);
	}
	public String getPageTemplateUrlDir(PageTemplateEntity pt){
		TemplateUtil tu=new TemplateUtil();
		return tu.getTemplateFileUrlPath(pt.getTemplateFilePath());
	}
	public String getPageTemplateRealDir(Long pageTemplateId){
		if(pageTemplateId==null)
			return "";
		IPageTemplateService pts=(IPageTemplateService) ServiceProxyFactory.getServiceProxy(PageTemplateServiceImpl.class);
		PageTemplateEntity pt=pts.getEntityById(pageTemplateId);
		return getPageTemplateRealDir(pt);
	}
	public String getPageTemplateRealDir(PageTemplateEntity pt){
		TemplateUtil tu=new TemplateUtil();
		return tu.getTemplateFileRealPath(pt.getTemplateFilePath());
	}
	public Object getPropertyValueByObject(Object obj,String propName) throws Exception{
		if(obj==null)
			return null;
		if(propName==null)
			return null;
		if(StringUtils.isBlank(propName))
			return null;
		String mtName="get"+propName.substring(0,1).toUpperCase()+propName.substring(1);
		Method[] mds=obj.getClass().getMethods();
		for(Method md:mds){
			if(md.getName().equals(mtName)){
				return md.invoke(obj, null);
			}
		}
		return null;
	}
	public String getEntityHtmlFields(String key){
		try {
			return com.ibpd.common.ProperCommon.readValue(ClassPath.getRealPath("/config/entityConfig.properties"), key);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return "";
	}
	/**
	 * 作为系统配置的缓存数据
	 */
	private static Map<String,String> sysPropertiesMap=new HashMap<String,String>();
	/**
	 * 配置变更后,重置系统配置缓存
	 * <b></b>
	 */
	public static void resetSysPropertiesCache(){
		sysPropertiesMap.clear();
	}
	public String getPropertiesValue(String key){
		return getPropertiesValue("system",key);
	}
	public String getPropertiesValue(String propFileName,String key){
		String value="";
		propFileName=propFileName.replace(".properties", "");
		value=sysPropertiesMap.get(key);
		if(!StringUtils.isBlank(value)){
			return value;
		}
		try {
			value=com.ibpd.common.ProperCommon.readValue(ClassPath.getRealPath("/config/"+propFileName+".properties"), key);
			if(StringUtils.isBlank(value)){
				ISystemProperService propServ=(ISystemProperService)ServiceProxyFactory.getServiceProxy(SystemProperServiceImpl.class);
				SystemProperEntity prop=propServ.getEntityByKey(key);
				if(prop!=null){
					value=prop.getValue();
					sysPropertiesMap.put(key, value);
				}
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return value;
	}
	public String getOtherQueryString(String query){
		String rtn="";
		if(query==null || StringUtil.isBlank(query))
			return rtn;
		//
		String[]  pms=query.split(";");
		for(String pm:pms){
			String[] singPmArray=pm.split(":");
			if(singPmArray.length==4){
				if(singPmArray[1].equals("eq")){
					rtn+=singPmArray[0]+"='"+singPmArray[2]+"' "+singPmArray[3]+" ";
				}else if(singPmArray[1].equals("like")){
					rtn+=singPmArray[0]+" like '%"+singPmArray[2]+"%' "+singPmArray[3]+" ";
				}else if(singPmArray[1].equals("gt")){
					rtn+=singPmArray[0]+" > '"+singPmArray[2]+"' "+singPmArray[3]+" ";
		 		}else if(singPmArray[1].equals("lt")){
					rtn+=singPmArray[0]+" < '"+singPmArray[2]+"' "+singPmArray[3]+" ";
				}
			}
		}
		if(StringUtil.isBlank(rtn)){
			return rtn;
		}else{
			return rtn.substring(0,rtn.length()-4);
		}
	}

}
