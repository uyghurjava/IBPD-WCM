package com.ibpd.henuocms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 站点
 * @author mg by qq:349070443
 *编辑于 2015-7-3 上午10:23:09
 */
@Entity
@Table(name="T_SubSite")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE,region="javaClassName")
public class SubSiteEntity extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * ID 
	 */
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**********************本项目中用到的字段*****************************/
	/**
	 * 站点群ID(暂时不用)
	 */
	@Column(name="f_siteGroupId",length=50,nullable=true)
	private Long siteGroupId=Long.parseLong("-1");
	/**
	 * 站点开始生效时间

	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_startDate",nullable=true)
	private Date startDate=new Date();
	/**
	 * 站点关闭时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_endDate",nullable=true)
	private Date endDate=new Date();
	/**
	 * 标题
	 */
	@Column(name="f_siteName",length=255,nullable=true)
	private String siteName=" ";
	/**
	 * 全称
	 */
	@Column(name="f_fullName",length=255,nullable=true)
	private String fullName=" ";
	/**
	 * 管理员

	 */
	@Column(name="f_manageUser",length=50,nullable=true)
	private String manageUser=" ";
	/**
	 * 域名,可以使用域名来访问,必须使用泛域名或者真实域名的指向,只写域名，不要写http://
	 */
	@Column(name="f_domainName",length=255,nullable=true)
	private String domainName = " ";
	/**
	 * 后台管理路径(暂时不用)
	 */
	@Column(name="f_manageUrl",length=255,nullable=true)
	private String manageUrl = " ";
	/**
	 * 描述
	 */
	@Column(name="f_description",length=255,nullable=true)
	private String description = " ";
	/**
	 *存放目录
	 */
	@Column(name="f_directory",length=255,nullable=true)
	private String directory = " ";
	/**
	 * 搜索关键字

	 */
	@Column(name="f_keywords",length=255,nullable=true)
	private String keywords = " ";
	/**
	 * 站点是否锁定
	 */
	@Column(name="f_isLocked",nullable=true)
	private Boolean isLocked=false;
	/**
	 * 创建时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createDate",nullable=true)
	private Date createDate=new Date();
	/**
	 * 审核时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_passedDate",nullable=true)
	private Date passedDate=new Date();
	/**
	 * 创建者
	 */
	@Column(name="f_createUser",length=50,nullable=true)
	private String createUser=" ";
	/**
	 * 排序
	 */
	@Column(name="f_order",nullable=true)
	private Integer order=0;
	/**
	 * 站点页面模板ID，如果模板库设置了相应的值，这里失效
	 */
	@Column(name="f_sitePageTemplateId",length=50,nullable=true)
	private Long sitePageTemplateId=Long.parseLong("-1");
	/**
	 * 站点样式模板ID，如果模板库设置了相应的值，这里失效
	 */
	@Column(name="f_siteStyleTemplateId",length=50,nullable=true)
	private Long siteStyleTemplateId=Long.parseLong("-1");
	/**
	 * 节点页面模板ID，如果模板库设置了相应的值，这里失效
	 */
	@Column(name="f_nodePageTemplateId",length=50,nullable=true)
	private Long nodePageTemplateId=Long.parseLong("-1");
	/**
	 * 节点样式模板ID，如果模板库设置了相应的值，这里失效
	 */
	@Column(name="f_nodeStyleTemplateId",length=50,nullable=true)
	private Long nodeStyleTemplateId=Long.parseLong("-1");
	/**
	 * 内容页面模板ID，如果模板库设置了相应的值，这里失效
	 */
	@Column(name="f_contentpageTemplateId",length=50,nullable=true)
	private Long contentpageTemplateId=Long.parseLong("-1");
	/**
	 * 内容样式模板ID，如果模板库设置了相应的值，这里失效
	 */
	@Column(name="f_contentStyleTemplateId",length=50,nullable=true)
	private Long contentStyleTemplateId=Long.parseLong("-1");
	/**
	 * 评论页面模板ID，如果模板库设置了相应的值，这里失效
	 */
	@Column(name="f_commentpageTemplateId",length=50,nullable=true)
	private Long commentpageTemplateId=Long.parseLong("-1");
	/**
	 * 评论页面模板ID，如果模板库设置了相应的值，这里失效
	 */
	@Column(name="f_commentStyleTemplateId",length=50,nullable=true)
	private Long commentStyleTemplateId=Long.parseLong("-1");
	/**
	 * 背景音乐,-1表示默认继承父级的

	 */
	@Column(name="f_bgMusic",length=255,nullable=true)
	private String bgMusic = " ";
	/**
	 * 标志,-1表示默认继承父级的

	 */
	@Column(name="f_logo",length=255,nullable=true)
	private String logo = " ";
	/**
	 * 图标
	 */
	@Column(name="f_icon",length=255,nullable=true)
	private String icon = " ";
	/**
	 * 代表性图片，主要用于栏目页面中的栏目图片显示
	 */
	@Column(name="f_map",length=255,nullable=true)
	private String map = " ";
	/**
	 * 横幅图片,-1表示默认继承父级的

	 */
	@Column(name="f_banner",length=255,nullable=true)
	private String banner = " ";
	/**
	 * 版权信息,-1表示默认继承父级的

	 */
	@Column(name="f_copyright",length=255,nullable=true)
	private String copyright = " ";
	/*******************以下是扩展多级站点的字段*****************************/
	/**
	 * 子节点数量,自动
	 */	
	@Column(name="f_childCount",nullable=true)
	private Integer childCount = 0;
	
	/*******************FTP部分********************************/
	/**
	 * FTP地址
	 */
	@Column(name="f_ftpAddress",length=255,nullable=true)
	private String ftpAddress = " ";
	/**
	 * ftp端口
	 */
	@Column(name="f_ftpPort",length=10,nullable=true)
	private String ftpPort = "21";
	/**
	 * ftp登录用户名
	 */
	@Column(name="f_ftpUserName",length=255,nullable=true)
	private String ftpUserName = " ";
	/**
	 * ftp密码
	 */
	@Column(name="f_ftpPassword",length=255,nullable=true)
	private String ftpPassword = " ";

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSiteGroupId() {
		return siteGroupId;
	}
	public void setSiteGroupId(Long siteGroupId) {
		this.siteGroupId = siteGroupId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getManageUser() {
		return manageUser;
	}
	public void setManageUser(String manageUser) {
		this.manageUser = manageUser;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public String getManageUrl() {
		return manageUrl;
	}
	public void setManageUrl(String manageUrl) {
		this.manageUrl = manageUrl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public Boolean getIsLocked() {
		return isLocked;
	}
	public void setIsLocked(Boolean isLocked) {
		this.isLocked = isLocked;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getPassedDate() {
		return passedDate;
	}
	public void setPassedDate(Date passedDate) {
		this.passedDate = passedDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public Long getNodePageTemplateId() {
		return nodePageTemplateId;
	}
	public void setNodePageTemplateId(Long nodePageTemplateId) {
		this.nodePageTemplateId = nodePageTemplateId;
	}
	public Long getNodeStyleTemplateId() {
		return nodeStyleTemplateId;
	}
	public void setNodeStyleTemplateId(Long nodeStyleTemplateId) {
		this.nodeStyleTemplateId = nodeStyleTemplateId;
	}
	public Long getContentpageTemplateId() {
		return contentpageTemplateId;
	}
	public void setContentpageTemplateId(Long contentpageTemplateId) {
		this.contentpageTemplateId = contentpageTemplateId;
	}
	public Long getContentStyleTemplateId() {
		return contentStyleTemplateId;
	}
	public void setContentStyleTemplateId(Long contentStyleTemplateId) {
		this.contentStyleTemplateId = contentStyleTemplateId;
	}
	public Long getCommentpageTemplateId() {
		return commentpageTemplateId;
	}
	public void setCommentpageTemplateId(Long commentpageTemplateId) {
		this.commentpageTemplateId = commentpageTemplateId;
	}
	public Long getCommentStyleTemplateId() {
		return commentStyleTemplateId;
	}
	public void setCommentStyleTemplateId(Long commentStyleTemplateId) {
		this.commentStyleTemplateId = commentStyleTemplateId;
	}
	public String getBgMusic() {
		return bgMusic;
	}
	public void setBgMusic(String bgMusic) {
		this.bgMusic = bgMusic;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getMap() {
		return map;
	}
	public void setMap(String map) {
		this.map = map;
	}
	public String getBanner() {
		return banner;
	}
	public void setBanner(String banner) {
		this.banner = banner;
	}
	public String getCopyright() {
		return copyright;
	}
	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}
	public Integer getChildCount() {
		return childCount;
	}
	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}
	public void setSitePageTemplateId(Long sitePageTemplateId) {
		this.sitePageTemplateId = sitePageTemplateId;
	}
	public Long getSitePageTemplateId() {
		return sitePageTemplateId;
	}
	public void setSiteStyleTemplateId(Long siteStyleTemplateId) {
		this.siteStyleTemplateId = siteStyleTemplateId;
	}
	public Long getSiteStyleTemplateId() {
		return siteStyleTemplateId;
	}
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	public String getDirectory() {
		return directory;
	}
	public String getFtpAddress() {
		return ftpAddress;
	}
	public void setFtpAddress(String ftpAddress) {
		this.ftpAddress = ftpAddress;
	}
	public String getFtpPort() {
		return ftpPort;
	}
	public void setFtpPort(String ftpPort) {
		this.ftpPort = ftpPort;
	}
	public String getFtpUserName() {
		return ftpUserName;
	}
	public void setFtpUserName(String ftpUserName) {
		this.ftpUserName = ftpUserName;
	}
	public String getFtpPassword() {
		return ftpPassword;
	}
	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}
	
}