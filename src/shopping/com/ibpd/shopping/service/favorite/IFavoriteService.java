package com.ibpd.shopping.service.favorite;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.FavoriteEntity;

public interface IFavoriteService extends IBaseService<FavoriteEntity> {
	Integer selectCount(FavoriteEntity fe);
	List<FavoriteEntity> getFavoriteListByAccount(Long accountId);
	void removeFavoriteByProductId(Long accountId,Long productId);
	void removeFavoriteByTenantId(Long accountId,Long tenantId);

}
