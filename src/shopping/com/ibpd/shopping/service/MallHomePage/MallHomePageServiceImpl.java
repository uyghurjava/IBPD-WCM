package com.ibpd.shopping.service.MallHomePage;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.shopping.entity.MallHomePageEntity;
@Service("mallHomePageService")
public class MallHomePageServiceImpl extends BaseServiceImpl<MallHomePageEntity> implements IMallHomePageService {
	public MallHomePageServiceImpl(){
		super();
		this.tableName="MallHomePageEntity";
		this.currentClass=MallHomePageEntity.class;
		this.initOK();
	}

	public List<MallHomePageEntity> getListByType(Integer type,Integer rowCount) {
		if(type==-1){
			return getList("from "+getTableName(),null,"order desc",100,0);
		}else{
			return getList("from "+getTableName()+" where type="+type,null,"order desc",rowCount,0);
		}
	}
}
