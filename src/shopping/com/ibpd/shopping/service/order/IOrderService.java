package com.ibpd.shopping.service.order;

import java.util.Date; 
import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.OrderEntity;
import com.ibpd.shopping.entity.OrderShipEntity;
import com.ibpd.shopping.entity.OrderdetailEntity;

public interface IOrderService extends IBaseService<OrderEntity> {
	boolean createOrder(OrderEntity order, List<OrderdetailEntity> orderdetailList,
			OrderShipEntity ordership) throws Exception ;
	List<OrderEntity> getListByAccount(String account,Integer pageSize,Integer pageIndex);
	List<OrderEntity> getRangeOrderList(Date startDate, Date endDate);
}
