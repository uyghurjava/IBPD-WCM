package com.ibpd.shopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_order")
public class OrderEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 订单状态
	 */
	/**
	 * 订单状态：下单
	 */
	public static final String order_status_init = "init";//已成功下单
	/**
	 * 订单状态：已审核
	 */
	public static final String order_status_pass = "pass";//已审核
	/**
	 * 订单状态：已发货
	 */
	public static final String order_status_send = "send";//已发货
	/**
	 * 订单状态：已收货
	 */
	public static final String order_status_sign = "sign";//已签收
	/**
	 * 订单状态：订单被取消
	 */
	public static final String order_status_cancel = "cancel";//已取消
	/**
	 * 订单状态：已归档
	 */
	public static final String order_status_file = "file";//已归档
	
	public static final String order_status_init_chinese = "已下单";
	public static final String order_status_pass_chinese = "已审核";
	public static final String order_status_send_chinese = "已发货";
	public static final String order_status_sign_chinese = "已签收";
	public static final String order_status_cancel_chinese = "已取消";
	public static final String order_status_file_chinese = "已归档";

	/**
	 * 订单支付状态 未支付
	 */
	public static final String order_paystatus_n = "n";// 未支付
	/**
	 * 订单支付状态 部分支付（后台未作支持）
	 */
	public static final String order_paystatus_p = "p";// 部分支付
	/**
	 * 订单支付状态 全部支付
	 */
	public static final String order_paystatus_y = "y";// 全部支付
	
	/**
	 * 订单是否缺货状态-缺货
	 */
	public static final String order_lowStocks_y = "y";//订单中存在商品缺货
	/**
	 * 订单是否缺货状态-不缺货
	 */
	public static final String order_lowStocks_n = "n";//不缺货
	
	/**
	 * 订单评论状态是否关闭
	 */
	public static String order_closedComment_y = "y";//已关闭
	/**
	 * 订单评论状态是否关闭
	 */
	public static String order_closedComment_n = "n";//未关闭
	/**
	 * 退货状态-申请提交
	 */
	public static String order_refund_status_init="init";
	/**
	 * 退货状态-卖家审核不通过
	 */
	public static String order_refund_status_unPass="unpass";
	/**
	 * 退货状态-卖家审核通过
	 */
	public static String order_refund_status_pass="passed";
	/**
	 * 退货状态-买家已发货
	 */
	public static String order_refund_status_sendPass="sendPass";
	/**
	 * 退货状态-卖家已收货并结档
	 */
	public static String order_refund_status_finish="finish";
	

	/**
	 * 主键 ID
	 */
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 下单人
	 */
	@Column(name="f_account",length=245,nullable=true)
	private String account;
	/**
	 * 付款类别 目前仅仅支持支付宝（zfb）
	 */
	@Column(name="f_payType",nullable=true)
	private Integer payType;
	/**
	 * 订单号
	 */
	@Column(name="f_orderNumber",length=13,nullable=true)
	private String orderNumber;
	/**
	 * 保留字段,不做说明
	 */
	@Column(name="f_carry",nullable=true)
	private Integer carry;
	/**
	 * 订单所属商户
	 */
	@Column(name="f_tenantId",nullable=true)
	private Long tenantId;
	/**
	 * 折扣
	 */
	@Column(name="f_rebate",nullable=true)
	private Double rebate;
	/**
	 * 下单时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createdate",nullable=true)
	private Date createdate;
	/**
	 * 订单状态更新时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_updatedate",nullable=true)
	private Date updatedate;
	/**
	 * 订单状态（参考‘订单状态’部分）
	 */
	@Column(name="f_status",length=10,nullable=true)
	private String status;
	/**
	 * 退货状态（参考‘退货状态’部分）
	 */
	@Column(name="f_refundStatus",length=45,nullable=true)
	private String refundStatus;
	/**
	 * 应付款（修改或折扣之后）
	 */
	@Column(name="f_amount",nullable=true)
	private Double amount;
	/**
	 * 运费
	 */
	@Column(name="f_fee",nullable=true)
	private Double fee;
	/**
	 * 订单总额（未修改或折扣之前）
	 */
	@Column(name="f_ptotal",nullable=true)
	private Double ptotal;
	/**
	 * 商品笔数（可理解为有几类商品）
	 */
	@Column(name="f_quantity",nullable=true)
	private Integer quantity;
	/**
	 * 付款状态 可参考订单支付状态，目前支持全部付款和未付款两种状态，分别对应Y和N
	 */
	@Column(name="f_paystatus",length=2,nullable=true)
	private String paystatus="n";
	/**
	 * 是否更新了总价（应付款）,如果更新了，该字段为Y，否则为n，用于后台手动打折
	 */
	@Column(name="f_updateAmount",length=2,nullable=true)
	private String updateAmount="n";
	/**
	 * 快递编码（对于app意义不大）
	 */
	@Column(name="f_expressCode",length=45,nullable=true)
	private String expressCode;
	/**
	 * 快递名称 即选择的快递方式
	 */
	@Column(name="f_expressName",length=45,nullable=true)
	private String expressName;
	/**
	 * 物流备注（如：周六日不收货）
	 */
	@Column(name="f_otherRequirement",length=45,nullable=true)
	private String otherRequirement;
	/**
	 * 订单备注（如：合并X笔订单）
	 */
	@Column(name="f_remark",length=545,nullable=true)
	private String remark;
	/**
	 * 快递单号，只有已发货的状态下才会有
	 */
	@Column(name="f_expressNo",length=45,nullable=true)
	private String expressNo;
	/**
	 * 快递公司名称
	 */
	@Column(name="f_expressCompanyName",length=45,nullable=true)
	private String expressCompanyName;
	/**
	 * 订单内是否有商品存在低库存 是为Y，否为n
	 */
	@Column(name="f_lowStocks",length=2,nullable=true)
	private String lowStocks="n";
	/**
	 * 保留字段，作为确认发货备注的字段，后台没做处理，可直接无视
	 */
	@Column(name="f_confirmSendProductRemark",length=100,nullable=true)
	private String confirmSendProductRemark="n";
	/**
	 * 退货说明，也就是说明为什么退货，由买家发起填写 
	 */
	@Column(name="f_refundRemark",length=100,nullable=true)
	private String refundRemark="";
	/**
	 * 订单关闭的说明
	 */
	@Column(name="f_closedComment",length=1,nullable=true)
	private String closedComment;
	/**
	 * 该订单送的积分数
	 */
	@Column(name="f_score",nullable=true)
	private Integer score;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public Integer getPayType() {
		return payType;
	}
	public void setPayType(Integer payType) {
		this.payType = payType;
	}
	public Integer getCarry() {
		return carry;
	}
	public void setCarry(Integer carry) {
		this.carry = carry;
	}
	public Double getRebate() {
		return rebate;
	}
	public void setRebate(Double rebate) {
		this.rebate = rebate;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRefundStatus() {
		return refundStatus;
	}
	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getFee() {
		return fee;
	}
	public void setFee(Double fee) {
		this.fee = fee;
	}
	public Double getPtotal() {
		return ptotal;
	}
	public void setPtotal(Double ptotal) {
		this.ptotal = ptotal;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getPaystatus() {
		return paystatus;
	}
	public void setPaystatus(String paystatus) {
		this.paystatus = paystatus;
	}
	public String getUpdateAmount() {
		return updateAmount;
	}
	public void setUpdateAmount(String updateAmount) {
		this.updateAmount = updateAmount;
	}
	public String getExpressCode() {
		return expressCode;
	}
	public void setExpressCode(String expressCode) {
		this.expressCode = expressCode;
	}
	public String getExpressName() {
		return expressName;
	}
	public void setExpressName(String expressName) {
		this.expressName = expressName;
	}
	public String getOtherRequirement() {
		return otherRequirement;
	}
	public void setOtherRequirement(String otherRequirement) {
		this.otherRequirement = otherRequirement;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getExpressNo() {
		return expressNo;
	}
	public void setExpressNo(String expressNo) {
		this.expressNo = expressNo;
	}
	public String getExpressCompanyName() {
		return expressCompanyName;
	}
	public void setExpressCompanyName(String expressCompanyName) {
		this.expressCompanyName = expressCompanyName;
	}
	public String getLowStocks() {
		return lowStocks;
	}
	public void setLowStocks(String lowStocks) {
		this.lowStocks = lowStocks;
	}
	public String getConfirmSendProductRemark() {
		return confirmSendProductRemark;
	}
	public void setConfirmSendProductRemark(String confirmSendProductRemark) {
		this.confirmSendProductRemark = confirmSendProductRemark;
	}
	public String getClosedComment() {
		return closedComment;
	}
	public void setClosedComment(String closedComment) {
		this.closedComment = closedComment;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public void setRefundRemark(String refundRemark) {
		this.refundRemark = refundRemark;
	}
	public String getRefundRemark() {
		return refundRemark;
	}
	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}
	public Date getUpdatedate() {
		return updatedate;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	public Long getTenantId() {
		return tenantId;
	}	
}
