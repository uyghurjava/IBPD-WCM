package com.ibpd.shopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_orderlog")
public class OrderLogEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String orderlog_accountType_w = "w";//日志类型：前�?
	public static final String orderlog_accountType_m = "m";//日志类型：后�?
	public static final String orderlog_accountType_p = "p";//日志类型：支付系统回�?
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_orderid",length=11,nullable=true)
	private String orderid;
	@Column(name="f_account",length=45,nullable=true)
	private String account;
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createdate",nullable=true)
	private Date createdate;
	@Column(name="f_content",length=1145,nullable=true)
	private String content;
	@Column(name="f_accountType",length=1,nullable=true)
	private String accountType;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
}
